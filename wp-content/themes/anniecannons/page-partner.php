<?php
/*
  Template Name: Partner
*/

get_header(); ?>




<!--
|--------------------------------------------------------------------------
|                                 PARTNER HERO
|--------------------------------------------------------------------------
-->

  <section id="partner-hero">
    <div class="partner-large-header" class="large-header">
      <div class="partner-header-container">
        <div class="partner-header-text-wrapper">
          <h1 class="partner-main-title">Partner</span></h1>
        </div> <!-- partner-header-text -->
      </div> <!-- partner-header-box -->
    </div> <!-- #partner-large-header -->
  </section>



<!--
|--------------------------------------------------------------------------
|                                 PARTNER
|--------------------------------------------------------------------------
-->

  <section id="partner-content">

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
          <div class="partner-content-container">
              <?php
                while ( have_posts() ) : the_post();

                  get_template_part( 'template-parts/content', 'basic' );

                endwhile; // End of the loop.
              ?>
          </div> <!-- /.partner-content-container -->
        </div> <!-- /.col -->
      </div> <!-- /.row -->
    </div> <!-- /.container-fluid -->

  </section>

<?php
get_footer();





