<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AnnieCannons
 */

?>
<?php wp_footer(); ?>

<!--
|--------------------------------------------------------------------------
|                                 FOOTER
|--------------------------------------------------------------------------
-->

  <footer>

    <div class="footer-logo-container">
      <h5><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo/black-stars-new.png">Annie Cannons</h5>
    </div> <!-- footer-stars-container -->

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4">
          <div class="footer-address-container">
            <span>AnnieCannons, Inc.</span> <br />
            <span>PO Box 170454</span> <br />
            <span>San Francisco, CA 94117</span>
          </div> <!-- footer-address-container -->
        </div> <!-- /.col -->


        <div class="col-xs-12 col-sm-4 col-md-4">
          <div class="footer-social-container">
            <a href="https://twitter.com/anniecannons" target="_blank"><i class="ion ion-social-twitter"></i></a>
            <a href="https://www.facebook.com/AnnieCannons/" target="_blank"><i class="ion ion-social-facebook"></i></a>
            <a href="https://github.com/anniecannons" target="_blank"><i class="ion ion-social-github"></i></a>
          </div> <!-- footer-social-container -->
        </div> <!-- /.col -->


        <div class="col-xs-12 col-sm-4 col-md-4">
          <div class="footer-contact-container">
            <div class="footer-phone-wrapper">
              <i class="icon-basic_smartphone"><a href="tel:415.780.4693"> 415.780.4693</a></i>
            </div> <!-- footer-phone-wrapper -->

            <div class="footer-email-wrapper">
              <i class="icon-mail_envelope"><a href="mailto:info@anniecannons.com"> info@anniecannons.com</a></i>
            </div> <!-- footer-email-wrapper -->
            <div class="privacy-policy"><a href="/privacy-policy">Privacy Policy</a></div>
          </div> <!-- footer-contact-container -->
        </div> <!-- /.col -->
      </div> <!-- /.row -->
    </div> <!-- /.container -->

  </footer>






<!--==== jQUERY ====-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!--==== BOOTSTRAP ====-->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!--==== SLICK CAROUSEL JS ====-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.js"></script>
<!--==== CUBE PORTFOLIO JS ====-->
  <script src="<?php bloginfo('template_directory'); ?>/assets/premium/cubeportfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
<!--==== AUTO SCROLL / EASING ====-->
  <script src="<?php bloginfo('template_directory'); ?>/assets/scripts/vendor/jquery.easing.min.js"></script>
<!--==== CUSTOM JS ====-->
  <script src="<?php bloginfo('template_directory'); ?>/assets/scripts/vendor/timeliner.min.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/scripts/main.min.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/scripts/bandaid-rob.js"></script>
<!--==== GOOGLE ANALYTICS ====-->
  <!-- <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='https://www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X','auto');ga('send','pageview');
  </script> -->

</body>
</html>
