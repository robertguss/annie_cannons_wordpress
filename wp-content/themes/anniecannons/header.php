<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AnnieCannons
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/favicon.ico">
<!--==== BOOTSTRAP ====-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!--==== ION ICONS CSS ====-->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />

<!--==== SLICK CAROUSEL CSS ====-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick.min.css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.9/slick-theme.min.css"/>

<!--==== CUBE PORTFOLIO CSS ====-->
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/assets/premium/cubeportfolio/cubeportfolio/css/cubeportfolio.min.css" />

<!--==== MAIN CSS ====-->
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory'); ?>/assets/styles/main.css">


<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'anniecannons' ); ?></a>



<!--
|--------------------------------------------------------------------------
|                                 NAVIGATION
|--------------------------------------------------------------------------
-->
<nav id="main-nav">
  <div class="main-nav-container">

    <div class="nav-logo-container">
      <a class="page-scroll" href="/"><h6 id="logo"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo/black-stars-new.png">AnnieCannons</h6></a>
    </div> <!-- nav-logo-container -->

    <div class="nav-links-container">
      <ul>
        <li>
          <a href="#">
            <span class="jquery-open" aria-hidden="true">
              <i class="ion ion-navicon"></i>
            </span>
          </a>
        </li>
        <li><a href="" class="top-nav-links">Volunteer</a></li>
        <li><a href="" class="top-nav-links donate-link-nav">Donate</a></li>
      </ul>
    </div> <!-- nav-links-container -->

  </div> <!-- main-nav-container -->
</nav>




<!--
|--------------------------------------------------------------------------
|                                 NAVIGATION OVERLAY
|--------------------------------------------------------------------------
-->

  <section id="nav-overlay">

    <!-- <div class="nav-overlay-x-close">
      <a href="#"><span class="glyphicon glyphicon-remove jquery-close" aria-hidden="true"></span></a>
    </div> --> <!-- nav-overlay-x-close -->

<div class="nav-overlay-container">

      <div class="nav-overlay-content-wrapper">
        <div class="nav-overlay-logo-div">
            <h4>Annie Cannons</h4>
        </div> <!-- nav-logo-div -->

        <ul>
          <li><a class="page-scroll" href="/">Home</a></li>
          <li><a class="page-scroll" href="/donate">Donate</a></li>
          <li><a class="page-scroll" href="/volunteer">Volunteer</a></li>
          <li><a class="page-scroll" href="/hire">Hire</a></li>
          <li><a class="page-scroll" href="/partner">Partner</a></li>
          <li><a class="page-scroll" href="/curriculum">Curriculum</a></li>
          <li><a class="page-scroll" href="/blog">Blog</a></li>
          <li><a class="page-scroll" href="/student-projects">Student Projects</a></li>
          <li><a class="page-scroll" href="/business-plan">Business Plan</a></li>
        </ul>
      </div> <!-- nav-overlay-content-wrapper -->

    </div> <!-- nav-overlay-container -->

  </section>
