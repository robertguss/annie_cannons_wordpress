<?php
/*
    Template Name: Home Page
 */

// Hero Section
  $hero_title                             = get_field('hero_title');
  $hero_sub_title                         = get_field('hero_sub_title');

// Mission Section
  $mission_1_title                        = get_field('mission_1_title');
  $mission_1_text                         = get_field('mission_1_text');
  $mission_2_title                        = get_field('mission_2_title');
  $mission_2_text                         = get_field('mission_2_text');
  $mission_3_title                        = get_field('mission_3_title');
  $mission_3_text                         = get_field('mission_3_text');

// Annie Cannons Section
  $annie_cannons                          = get_field('annie_cannons');

// Get Involved Section
  $get_involved_top_left_box_title        = get_field('get_involved_top_left_box_title');
  $get_involved_top_left_box_text         = get_field('get_involved_top_left_box_text');
  $get_involved_bottom_left_box_title     = get_field('get_involved_bottom_left_box_title');
  $get_involved_bottom_left_box_text      = get_field('get_involved_bottom_left_box_text');
  $get_involved_top_right_box_title       = get_field('get_involved_top_right_box_title');
  $get_involved_top_right_box_text        = get_field('get_involved_top_right_box_text');
  $get_involved_bottom_right_box_title    = get_field('get_involved_bottom_right_box_title');
  $get_involved_bottom_right_box_text     = get_field('get_involved_bottom_right_box_text');


get_header(); ?>


<!--=======
    HERO
===========-->

<section id="hero">

  <div class="hero-container">
    <video poster="<?php bloginfo('stylesheet_directory'); ?>/assets/images/hero/video/aysegul-poster.jpg" preload="metadata" loop autoplay muted class="hero-video">
      <source src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/hero/video/aysegul-loop-high.mp4" type="video/mp4">
    </video>
  </div> <!-- hero-container -->

</section>


  <!--=======
    MISSION
===========-->

<section id="mission">

  <div class="mission-container edge--top--reverse">
    <h2 class="mission-header"><?php echo($hero_title) ?></h2>
    <p class="mission-copy"><?php echo($hero_sub_title) ?></p>

    <div class="video-play-btn-container">
      <a href="" class="open-modal"><i class="icon-music_play_button"></i></a>
    </div> <!-- video-modal-container -->
  </div> <!-- mission-container -->

</section>



<section id="video-modal-section">
  <div class="video-modal-container">
    <div class="video-modal-iframe-wrapper">
      <iframe width="640" height="360" src="https://www.youtube.com/embed/0SJIgTLe0hc?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
    </div> <!-- video-modal-iframe-wrapper -->
  </div> <!-- video-modal-container -->
</section>

  <!--=======
    MISSION
===========-->

<section id="problem-approach-aspiration">

  <!-- PROBLEM
  =============================== -->
  <div class="container-fluid problem-container">
    <div class="row">

      <div class="col-sm-12 col-md-6 columns">
        <div class="problem-header-container">
          <h4 class="text-center"><?php echo($mission_1_title); ?></h4>
        </div> <!-- problem-header-container -->
      </div> <!-- columns -->

      <div class="col-sm-12 col-md-6 columns">
        <div class="problem-copy-container">
          <p><?php echo($mission_1_text); ?></p>
        </div> <!-- problem-copy-container -->
      </div> <!-- columns -->

    </div> <!-- row -->
  </div> <!-- container -->



  <!-- APPROACH
  =============================== -->
  <div class="container-fluid approach-container">
    <div class="row">

      <div class="col-sm-12 col-md-6 col-md-push-6 columns">
        <div class="approach-header-container">
          <h4 class="text-center"><?php echo($mission_2_title); ?></h4>
        </div> <!-- approach-header-container -->
      </div> <!-- columns -->

      <div class="col-sm-12 col-md-6 col-md-pull-6 columns">
        <div class="approach-copy-container">
          <p><?php echo($mission_2_text); ?></p>
        </div> <!-- approach-copy-container -->
      </div> <!-- columns -->

    </div> <!-- row -->
  </div> <!-- container -->



  <!-- ASPIRATION
  =============================== -->
  <div class="container-fluid aspiration-container">
    <div class="row">

      <div class="col-sm-12 col-md-6 columns">
        <div class="aspiration-header-container">
          <h4 class="text-center"><?php echo($mission_3_title); ?></h4>
        </div> <!-- aspiration-header-container -->
      </div> <!-- columns -->

      <div class="col-sm-12 col-md-6 columns">
        <div class="aspiration-copy-container">
          <p><?php echo($mission_3_text); ?></p>
        </div> <!-- aspiration-copy-container -->
      </div> <!-- columns -->

    </div> <!-- row -->
  </div> <!-- container -->
</section>

  <!--
|--------------------------------------------------------------------------
|                                 ANNIE CANNONS
|--------------------------------------------------------------------------
-->

  <section id="annie-cannons">

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="annie-cannon-bio-wrapper">
            <p><?php echo($annie_cannons); ?></p>
          </div> <!-- /.annie-cannon-bio-wrapper -->
        </div> <!-- /.col -->
      </div> <!-- /.row -->
    </div> <!-- /.container-fluid -->

  </section>



<!--=======
GET INVOLVED
===========-->

  <div class="get-involved-header-container">
    <h4 class="text-center">Get Involved</h4>
    <div class="get-involved-section-header-line gradient"></div>
  </div> <!-- get-involved-header-container -->

  <section id="get-involved">
    <div class="container-fluid">
      <!--feature border box start-->
      <div class="row">

      <!-- PARTNER
      ===================-->
        <div class="col-sm-12 col-lg-4">
          <a href="/partner">
            <div class="get-involved-container trafficking-container">
              <div class="get-involved-icon">
                <i class="icon-attachments"></i>
              </div>
              <div class="get-involved-header-title-container">
                <h4 class="get-involved-header-title trafficking-title"><?php echo($get_involved_top_left_box_title); ?></h4>
              </div>
              <div class="get-involved-copy">
                <p><?php echo($get_involved_top_left_box_text); ?></p>
              </div>
            </div> <!-- /.partner-container -->
          </a>

      <!-- DONATE
      ===================-->
          <a href="/donate">
            <div class="get-involved-container donate-container">
              <div class="get-involved-icon">
                <i class="icon-ecommerce_gift"></i>
              </div>
              <div class="get-involved-header-title-container">
                <h4 class="get-involved-header-title donate-title"><?php echo($get_involved_bottom_left_box_title); ?></h4>
              </div>
              <div class="get-involved-copy">
                <p><?php echo($get_involved_bottom_left_box_text); ?></p>
              </div>
            </div> <!-- /.donate-container -->
          </a>
        </div> <!-- col -->


        <div class="col-sm-12 col-lg-4 text-center get-involved-center-column">
          <div class="get-involved-logo-container">
            <!-- <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/logo/logo.png" alt=""> -->
          </div>
        </div>

      <!-- VOLUNTEER
        ===================-->
        <div class="col-sm-12 col-lg-4">
          <a href="/volunteer">
            <div class="get-involved-container volunteer-container">
              <div class="get-involved-icon">
                <i class="icon-heart2"></i>
              </div>
              <div class="get-involved-header-title-container">
                <h4 class="get-involved-header-title volunteer-title"><?php echo($get_involved_top_right_box_title); ?></h4>
              </div>
              <div class="get-involved-copy">
                <p><?php echo($get_involved_top_right_box_text); ?></p>
              </div>
            </div> <!-- /.volunteer-container -->
          </a>


        <!-- HIRE
        ===================-->
          <a href="/hire">
            <div class="get-involved-container hire-container">
              <div class="get-involved-icon">
                <i class="icon-briefcase2"></i>
              </div>
              <div class="get-involved-header-title-container">
                <h4 class="get-involved-header-title hire-title"><?php echo($get_involved_bottom_right_box_title); ?></h4>
              </div>
              <div class="get-involved-copy">
                <p><?php echo($get_involved_bottom_right_box_text); ?></p>
              </div>
            </div>  <!-- /.hire-container -->
          </a>
        </div> <!-- col -->

      </div> <!-- row -->
    </div> <!-- container -->
  </section>



<!--
|--------------------------------------------------------------------------
|                                 STUDENT TESTIMONIALS
|--------------------------------------------------------------------------
-->

  <section id="student-testimonials">
    <div class="container-fluid">

      <div class="row">
        <div class="col-sm-12 col-md-6">
          <div class="student-testimonials-header">
            <h3>What our students are saying...</h3>
            <div class="student-testimonials-hr gradient"></div>
          </div> <!-- student-testimonials-header -->
        </div> <!-- col-sm-12 col-md-6 -->


        <div class="col-sm-12 col-md-6">

        <div class="testimonials-wrapper">
            <div class="student-testimonials-testimonials">

              <div class="tesimonials tesimonial-1">
                <p>“I would highly recommend the training because I believe that every single girl in San Francisco and in this world deserves the human right of education…I think that AnnieCannons is going to change their lives, that it is going to be super helpful because you can have the skills and have the support that you need to follow your dreams and to have a better life for yourself and your family.”</p>
              </div> <!-- tesimonials tesimonial-1 -->

              <div class="tesimonials tesimonial-2">
                <p>“Reason #1 I joined this training is to help my community.”</p>
              </div> <!-- tesimonials tesimonial-2 -->

              <div class="tesimonials tesimonial-3">
                <p>“Sometimes [this training] gets complicated and sometimes it’s really fun.”</p>
              </div> <!-- tesimonials tesimonial-3 -->

              <div class="tesimonials tesimonial-4">
                <p>“I want to use the training to find solutions for the things that are happening in my community like homelessness, housing prices, sex trafficking and domestic violence.”</p>
              </div> <!-- tesimonials tesimonial-4 -->

              <div class="tesimonials tesimonial-5">
                <p>“Thank you for giving me this opportunity and for the love and support that I get. I feel that this is gonna change my life and my community."</p>
              </div> <!-- tesimonials tesimonial-5 -->

            </div> <!-- student-testimonials-testimonials -->
          </div> <!-- testimonials-wrapper -->
        </div> <!-- col-sm-12 col-md-6 -->

      </div> <!-- row -->
    </div> <!-- container -->
  </section>





<!--
|--------------------------------------------------------------------------
|                                 4 BLOCK GRID
|--------------------------------------------------------------------------
-->

  <section id="four-block-grid">
    <div class="container-fluid">
      <div class="row">

        <div class="col-sm-12 col-md-3">
          <a href="/curriculum" class="four-block-grid-a curriculum-a">
            <div class="four-block-grid-block-wrapper four-grid-curriculum">
              <i class="icon-computer_imac_thick"></i>
              <h5 class="text-center">Curriculum</h5>
            </div> <!-- /.four-block-grid-block-wrapper -->
          </a>
        </div> <!-- /.col -->


        <div class="col-sm-12 col-md-3">
          <a href="/blog" class="four-block-grid-a blog-a">
            <div class="four-block-grid-block-wrapper four-grid-blog">
              <i class="icon-chat"></i>
              <h5 class="text-center">Blog</h5>
            </div> <!-- /.four-block-grid-block-wrapper -->
          </a>
        </div> <!-- /.col -->


        <div class="col-sm-12 col-md-3">
          <a href="/student-projects" class="four-block-grid-a student-projects-a">
            <div class="four-block-grid-block-wrapper four-grid-student-projects">
              <i class="icon-pencil"></i>
              <h5 class="text-center">Student Projects</h5>
            </div> <!-- /.four-block-grid-block-wrapper -->
          </a>
        </div> <!-- /.col -->


        <div class="col-sm-12 col-md-3">
          <a href="/business-plan" class="four-block-grid-a business-plan-a">
            <div class="four-block-grid-block-wrapper four-grid-business-plan">
              <i class="icon-circle-compass"></i>
              <h5 class="text-center">Business Plan</h5>
            </div> <!-- /.four-block-grid-block-wrapper -->
          </a>
        </div> <!-- /.col -->

      </div> <!-- /.row -->
    </div> <!-- /.container-fluid -->
  </section>




  <!--=======
    PARTNERS
===========-->

  <section id="partners">

  <!-- CAROUSEL
  =================================-->

    <div class="partners-header-container">
      <h4 class="text-center">Partners</h4>
      <div class="partners-section-header-line gradient"></div>
    </div> <!-- partners-header-container -->


    <section id="partner-categories">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-4">
            <h5 class="text-center">Intake</h5>
          </div> <!-- /.col -->
          <div class="col-sm-12 col-md-4">
            <h5 class="text-center">Curriculum</h5>
          </div> <!-- /.col -->
          <div class="col-sm-12 col-md-4">
            <h5 class="text-center">Infrastructure</h5>
          </div> <!-- /.col -->
        </div> <!-- /.row -->
      </div> <!-- /.container-fluid -->
    </section>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">

          <div class="partner-logos">
            <!-- Curriculum -->
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/1.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/2.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/3.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/4.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/5.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/6.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/7.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/8.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/9.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/10.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/curriculum/11.png">
            <!-- Infrastructure -->
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/infrastructure/1.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/infrastructure/2.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/infrastructure/3.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/infrastructure/4.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/infrastructure/5.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/infrastructure/6.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/infrastructure/7.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/infrastructure/8.png">
            <!-- Intake -->
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/intake/1.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/intake/2.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/intake/3.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/intake/4.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/intake/5.png">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/partners/intake/6.png">

          </div> <!-- /.partner-logos -->
        </div> <!-- /.col -->
      </div> <!-- /.row -->
    </div> <!-- /.container -->

  </section>






  <!--=======
    TEAM
===========-->

  <section id="team">

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 columns">

          <div class="team-header-container">
            <h4 class="text-center">Team</h4>
            <div class="team-section-header-line gradient"></div>
          </div> <!-- team-header-container -->



          <div id="js-grid-lightbox-gallery" class="cbp">

          <!--==== Laura Hackney ====-->
              <div class="cbp-item web-design graphic print motion">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/laura.html" class="cbp-caption cbp-singlePageInline" data-title="LAURA HACKNEY<br>COFOUNDER" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/laura.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">LAURA HACKNEY</div>
                                  <div class="cbp-l-caption-desc">COFOUNDER</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>

            <!--==== Jessica Hubley ====-->
              <div class="cbp-item web-design logos identity graphic">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/jessica.html" class="cbp-caption cbp-singlePageInline" data-title="JESSICA HUBLEY<br>COFOUNDER" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/jessica.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">JESSICA HUBLEY</div>
                                  <div class="cbp-l-caption-desc">COFOUNDER</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>

            <!--==== Aysegul Yonet ====-->
              <div class="cbp-item graphic print identity">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/aysegul.html" class="cbp-caption cbp-singlePageInline" data-title="AYSEGUL YONET<br>CTO" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/aysegul.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">AYSEGUL YONET</div>
                                  <div class="cbp-l-caption-desc">DIRECTOR &amp; CTO</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>

            <!--==== Mckinley Smith ====-->
              <div class="cbp-item web-design motion logos">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/mckinley.html" class="cbp-caption cbp-singlePageInline" data-title="MCKINLEY SMITH<br>ADVISOR" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/mckinley.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">MCKINLEY SMITH</div>
                                  <div class="cbp-l-caption-desc">ADVISOR</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>

            <!--==== Patti Ordonez ====-->
              <div class="cbp-item identity graphic print">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/patti.html" class="cbp-caption cbp-singlePageInline" data-title="PATTI ORDOÑEZ<br>ADVISOR" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/patti.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">PATTI ORDOÑEZ</div>
                                  <div class="cbp-l-caption-desc">ADVISOR</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>

            <!--==== Minh Dang ====-->
              <div class="cbp-item motion print logos web-design">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/minh.html" class="cbp-caption cbp-singlePageInline" data-title="MINH DANG<br>ADVISOR" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/minh.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">MINH DANG</div>
                                  <div class="cbp-l-caption-desc">ADVISOR</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>

            <!--==== Tamika Butler ====-->
              <div class="cbp-item graphic logos">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/tamika.html" class="cbp-caption cbp-singlePageInline" data-title="TAMIKA BUTLER<br>ADVISOR" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/tamika.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">TAMIKA BUTLER</div>
                                  <div class="cbp-l-caption-desc">ADVISOR</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>

            <!--==== Bradley Myles ====-->
              <div class="cbp-item identity print logos motion">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/bradley.html" class="cbp-caption cbp-singlePageInline" data-title="BRADLEY MYLES<br>ADVISOR" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/bradley.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">BRADLEY MYLES</div>
                                  <div class="cbp-l-caption-desc">ADVISOR</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>



            <!--==== Mary Cunningham Agee ====-->
              <div class="cbp-item identity print logos motion">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/mary.html" class="cbp-caption cbp-singlePageInline" data-title="MARY CUNNINGHAM AGEE<br>ADVISOR" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/mary.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">MARY CUNNINGHAM AGEE</div>
                                  <div class="cbp-l-caption-desc">ADVISOR</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>


            <!--==== Isaac Adams ====-->
              <div class="cbp-item identity print logos motion">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/isaac.html" class="cbp-caption cbp-singlePageInline" data-title="ISAAC ADAMS<br>ADVISOR" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/isaac.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">ISAAC ADAMS</div>
                                  <div class="cbp-l-caption-desc">ADVISOR</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>

            <!--==== Richard Lui ====-->
              <div class="cbp-item identity print logos motion">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/richard.html" class="cbp-caption cbp-singlePageInline" data-title="RICHARD LUI<br>ADVISOR" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/richard.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">RICHARD LUI</div>
                                  <div class="cbp-l-caption-desc">ADVISOR</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>

            <!--==== Kristin Brey ====-->
              <div class="cbp-item identity print logos motion">
                  <a href="<?php bloginfo('stylesheet_directory'); ?>/assets/team-profiles/kristin.html" class="cbp-caption cbp-singlePageInline" data-title="RICHARD LUI<br>ADVISOR" rel="nofollow">
                      <div class="cbp-caption-defaultWrap">
                          <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/team/kristin.jpg" alt="">
                      </div>
                      <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                              <div class="cbp-l-caption-body">
                                  <div class="cbp-l-caption-title">KRISTIN BREY</div>
                                  <div class="cbp-l-caption-desc">ADVISOR</div>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>
          </div> <!-- js-grid-lightbox-gallery -->

        </div> <!-- col -->
      </div> <!-- row -->
    </div> <!-- container -->
  </section>



<!--
|--------------------------------------------------------------------------
|                                 SLIDE SHOW
|--------------------------------------------------------------------------
-->

  <section id="home-page-slideshow">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 slideshow-col">

          <div class="our-inspiration-header-container">
            <h4 class="text-center">Our Inspiration</h4>
            <div class="our-inspiration-section-header-line gradient"></div>
          </div> <!-- our-inspiration-header-container -->

          <div class="home-page-slideshow">

            <div class="slide-container">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/slideshow/widescreen/annie-easley.jpg">
              <div class="slide-caption">
                <p>Dr. Annie Easley was raised by a single mother in the segregated South, but she still earned a Ph.D. in mathematics and joined NASA; her innovations enabled spaceflight through rocket technology and hybrid car batteries.</p>
              </div>
            </div>


            <div class="slide-container">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/slideshow/widescreen/hedy-lamarr.jpg">
              <div class="slide-caption">
                <p>Hedy Lamarr, remembered for her beauty and films, actually invented “spread spectrum” radio technology – which allowed the US to encrypt military messages during World War II and which formed the basis for modern wireless technology.</p>
              </div>
            </div>


            <div class="slide-container">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/slideshow/widescreen/mae-jemison.jpg">
              <div class="slide-caption">
                <p>Dr. Mae Jemison went to Stanford at 16 and trained as a physician before being recruited to NASA, where she became the first black woman in space. She later founded a successful biotech company and a foundation for underprivileged youth.</p>
              </div>
            </div>


            <div class="slide-container">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/slideshow/widescreen/chien-shiung-wu.jpg">
              <div class="slide-caption">
                <p>Dr. Chien-Shiung Wu was a nuclear physicist whose insights illuminated the behavior of decaying atoms and who, during the Manhattan Project, developed the process for separating uranium isotopes by weight.</p>
              </div>
            </div>


            <div class="slide-container">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/slideshow/widescreen/rosalind-franklin.jpg">
              <div class="slide-caption">
                <p>Dr. Rosalind Franklin was a chemist and X-ray Crystallographer whose imaging and study of DNA revealed its double-helix structure & phosphate backbone. Though she was fundamental to identifying DNA’s double-helix structure, her name is rarely associated with the Nobel-prize winning discovery.</p>
              </div>
            </div>


            <div class="slide-container">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/slideshow/widescreen/ellen-ochoa.jpg">
              <div class="slide-caption">
                <p>Dr. Ellen Ochoa is a pilot, astronaut and electrical engineer who holds three patents on optical engineering systems; she’s logged nearly a thousand hours in orbit, where, as a classical flutist, she was the first to experiment with music in space.</p>
              </div>
            </div>


            <div class="slide-container">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/slideshow/widescreen/flossie-wong-staal.jpg">
              <div class="slide-caption">
                <p>Dr. Flossie Wong-Staal helped identify the virus that causes AIDS – and one that causes cancer – as well as cloning and mapping the HIV genome, which paved the way for her invention of the first HIV test.</p>
              </div>
            </div>


            <div class="slide-container">
              <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/slideshow/widescreen/annie-jump-cannon.jpg">
              <div class="slide-caption">
                <p>Annie Jump Cannon first discerned the categories of stars by observing their spectral emissions, enabling women she worked with to discover that those categories reflected the chemical composition and lifecycle of stars – a breakthrough that forged modern astronomy but for which this group of remarkable scientists is rarely credited.</p>
              </div>
            </div>

          </div> <!-- /.col -->
        </div> <!-- /.row -->
      </div> <!-- /.container-fluid -->
    </div> <!-- /.home-page-slideshow -->
  </section>



<?php
get_footer();

