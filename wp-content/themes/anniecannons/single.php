<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package AnnieCannons
 */

get_header(); ?>

<!--
|--------------------------------------------------------------------------
|                                 BLOG POST SINGLE HERO
|--------------------------------------------------------------------------
-->

  <section id="blog-post-single-hero">
    <div class="blog-post-single-large-header" class="large-header">
      <div class="blog-post-single-header-container">
        <div class="blog-post-single-header-text-wrapper">
          <h1 class="blog-post-single-main-title">Post</span></h1>
        </div> <!-- blog-post-single-header-text -->
      </div> <!-- blog-post-single-header-box -->
    </div> <!-- #blog-post-single-large-header -->
  </section>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content-single', get_post_format() );

		endwhile; // End of the loop.

      echo('<div class="container">');
      echo('<div class="row">');
      echo('<div class="col-sm-4 col-sm-offset-4">');
      echo('<br />');
        the_posts_navigation();
      echo('</div> <!-- col -->');
      echo('</div> <!-- row -->');
      echo('</div> <!-- container -->');
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
