<?php
/*
  Template Name: Curriculum
*/

get_header(); ?>



  <!--
|--------------------------------------------------------------------------
|                                 CURRICULUM HERO
|--------------------------------------------------------------------------
-->

  <section id="curriculum-hero">
    <div id="large-header" class="large-header">
      <canvas id="demo-canvas"></canvas>
      <h1 class="main-title"><span class="thin">Curriculum</span></h1>
    </div>


  </section>

    <script src="<?php bloginfo('template_directory'); ?>/assets/scripts/vendor/TweenLite.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/scripts/vendor/EasePack.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/scripts/vendor/rAF.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/scripts/vendor/demo-1.js"></script>

  <!--
|--------------------------------------------------------------------------
|                                 CURRICULUM TIMELINE
|--------------------------------------------------------------------------
-->

  <section id="curriculum-timeline">

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="curriculum-header-copy-container">
            <h4 class="text-center">Curriculum Summary and Outline</h4>

            <p>Our curriculum builds on donated and publicly available resources with a track record of resounding success.  We curate these resources and deliver them in a supportive, trauma-informed environment. We use examples, teaching methods, and practice projects that our students identify with and that teach critical skills they need to maintain the economic power they earn.</p>
            <br>
            <p>Every student learns at a different pace, and practice is critical to success in software development. We gradually transition students from building practice projects to building paying client projects on the timeline that works for them.  (The timelines below are approximations)</p>
          </div> <!-- curriculum-header-copy-container -->
        </div> <!-- col -->
      </div> <!-- row -->
    </div> <!-- container -->



    <!-- TIMELINE
    =====================================-->

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div id="timeline" class="timeline-container">
            <button class="timeline-toggle">+ expand all</button>

            <div class="timeline-wrapper">
              <h3 class="timeline-time">Digital Literacy</h3>
              <dl class="timeline-series">
                <dt class="timeline-event" id="event01"><a>6 weeks</a></dt>
                <dd class="timeline-event-content" id="event01EX">
                  <ul>
                    <li>Typing</li>
                    <li>Personal Finance</li>
                    <li>How Computers Work</li>
                    <li>How Software Works</li>
                    <li>Critical Software Programs</li>
                    <li>How the Internet Works</li>
                    <li>Software Development Lifecycle</li>
                    <li>Software Testing</li>
                  </ul>
                  <p>*<em>upon completion of this module, students can earn up to $30 per hour as testers.</em></p>
                </dd>
              </dl>
            </div> <!-- timeline-wrapper -->

            <div class="timeline-wrapper">
              <h3 class="timeline-time">Software Development</h3>
              <dl class="timeline-series">
                <dt class="timeline-event" id="event02"><a>4 months (approx.)</a></dt>
                <dd class="timeline-event-content" id="event03EX">
                  <ul>
                    <li>HTML/CSS</li>
                    <li>Core Javascript</li>
                  </ul>
                </dd>
              </dl>
            </div> <!-- timeline-wrapper -->

            <div class="timeline-wrapper">
              <h3 class="timeline-time">Design Track</h3>
              <dl class="timeline-series">
                <dt class="timeline-event" id="event03"><a>2 months (approx.)</a></dt>
                <dd class="timeline-event-content" id="event03EX">
                  <ul>
                    <li>Principles of UX Design</li>
                    <li>Visual Design Principles</li>
                    <li>Advanced CSS</li>
                    <li>Design Tools</li>
                  </ul>
                </dd>
              </dl>
            </div> <!-- timeline-wrapper -->

            <div class="timeline-wrapper">
              <h3 class="timeline-time">Full Stack</h3>
              <dl class="timeline-series">
                <dt class="timeline-event" id="event04"><a>4 to 8 months</a></dt>
                <dd class="timeline-event-content" id="event03EX">
                  <ul>
                    <li>Advanced Javascript</li>
                    <li>MEAN Stack</li>
                    <li>Python</li>
                    <li>Mobile Applications</li>
                  </ul>
                </dd>
              </dl>
            </div> <!-- timeline-wrapper -->

          </div> <!-- id="timeline" class="timeline-container" -->
        </div> <!-- col -->
      </div> <!-- row -->
    </div> <!-- container -->




  </section>

<?php
get_footer();





