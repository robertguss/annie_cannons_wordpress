<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AnnieCannons
 */

?>

<!--
|--------------------------------------------------------------------------
|                                 BLOG POST SINGLE
|--------------------------------------------------------------------------
-->

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


  <section id="blog-post-single">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
          <div class="blog-post-single-left-column-container">

            <?php the_post_thumbnail(); ?>


            <h5><?php the_title(); ?></h5>
            <?php
              if ( 'post' === get_post_type() ) : ?>
              <span><?php the_author(); ?> |</span><span> <?php the_date(); ?></span>
            <?php endif; ?>


            <?php the_content(); ?>


              <div class="post-prev-next-home-btns-container">
              <div class="prev-post">
                <?php previous_post_link( '%link', '<i class="icon-arrows_slim_left"></i> Prev post', TRUE ); ?>
              </div> <!-- prev-post -->
              <div class="blog-home-icon">
                <a href="/blog"><i class="icon-home"></i></a>
              </div> <!-- blog-home-icon -->
              <div class="next-post">
                <?php next_post_link( '%link', 'Next post <i class="icon-arrows_slim_right"></i>', TRUE ); ?>
              </div> <!-- prev-post -->
            </div> <!-- post-prev-next-home-btns-container -->
          </div> <!-- blog-post-single-left-column-container -->
        </div> <!-- col -->

        <div class="col-sm-12 col-md-4">
          <div class="blog-post-single-right-column-container">

          </div> <!-- blog-post-single-right-column-container -->
        </div> <!-- col -->
      </div> <!-- row -->
    </div> <!-- container -->

  </section>

</article><!-- #post-## -->









