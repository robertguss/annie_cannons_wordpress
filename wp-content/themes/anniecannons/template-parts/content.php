<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AnnieCannons
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">


    <!-- RIGHT CONTAINER & TEXT -->
      <div class="col-sm-8 col-sm-offset-2">
        <div class="blog-post-right-container">
          <h5 class="blog-post-right-container-title">
            <?php the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a>' ); ?>
          </h5>


      		<?php
      		if ( 'post' === get_post_type() ) : ?>
            <span><?php the_author(); ?> |</span><span> <?php the_date(); ?></span>
      		<?php endif; ?>

          <?php the_excerpt(); ?>
      </div> <!-- blog-post-right-container -->
    </div> <!-- col -->
	</header><!-- .entry-header -->


</article><!-- #post-## -->
