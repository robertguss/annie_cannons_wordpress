<?php
/*
  Template Name: Volunteer
*/

get_header(); ?>



   <!--
|--------------------------------------------------------------------------
|                                 VOLUNTEER HERO
|--------------------------------------------------------------------------
-->

  <section id="volunteer-hero">
    <div class="volunteer-large-header" class="large-header">
      <div class="volunteer-header-container">
        <div class="volunteer-header-text-wrapper">
          <h1 class="volunteer-main-title">Volunteer</span></h1>
        </div> <!-- volunteer-header-text -->
      </div> <!-- volunteer-header-box -->
    </div> <!-- #volunteer-large-header -->
  </section>



<!--
|--------------------------------------------------------------------------
|                                 VOLUNTEER
|--------------------------------------------------------------------------
-->

  <section id="volunteer-content">

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
          <div class="volunteer-content-container">
              <?php
                while ( have_posts() ) : the_post();

                  get_template_part( 'template-parts/content', 'basic' );

                endwhile; // End of the loop.
              ?>
          </div> <!-- /.volunteer-content-container -->
        </div> <!-- /.col -->
      </div> <!-- /.row -->
    </div> <!-- /.container-fluid -->

  </section>

<?php
get_footer();





