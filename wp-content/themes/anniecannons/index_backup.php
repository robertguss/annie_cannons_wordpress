<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AnnieCannons
 */

get_header(); ?>


<!--
|--------------------------------------------------------------------------
|                                 BLOG HERO
|--------------------------------------------------------------------------
-->

  <section id="blog-hero">

    <div id="large-header" class="large-header">
      <canvas id="demo-canvas"></canvas>

      <div class="blog-header-container">
        <div class="blog-header-text-wrapper">
          <h1 class="blog-main-title">Blog</span></h1>
        </div> <!-- blog-header-text -->
      </div> <!-- blog-header-box -->

    </div> <!-- #large-header -->

  </section>


  <script src="<?php bloginfo('template_directory'); ?>/assets/scripts/vendor/rAF.js"></script>
  <script src="<?php bloginfo('template_directory'); ?>/assets/scripts/vendor/demo-2.js"></script>


<!--
|--------------------------------------------------------------------------
|                                 BLOG POSTS
|--------------------------------------------------------------------------
-->

  <section id="blog-posts">

    <?php
      while ( have_posts() ) : the_post();

        /*
         * Include the Post-Format-specific template for the content.
         * If you want to override this in a child theme, then include a file
         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
         */
        echo('<div class="blog-post-1-wrapper">');
        echo('<div class="container">');
        echo('<div class="row">');

        get_template_part( 'template-parts/content', get_post_format() );

        echo('</div> <!-- row -->');
        echo('</div> <!-- container -->');
        echo('</div> <!-- blog-post-1-wrapper -->');

      endwhile;

      echo('<div class="container">');
      echo('<div class="row">');
      echo('<div class="col-sm-12">');
      echo('<br />');
        the_posts_navigation();
      echo('</div> <!-- col -->');
      echo('</div> <!-- row -->');
      echo('</div> <!-- container -->');
    ?>

  </section>

<?php
echo('<br />');
echo('<br />');
get_footer();
