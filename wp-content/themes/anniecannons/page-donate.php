<?php
/*
  Template Name: Donate
*/

get_header(); ?>



  <!--
|--------------------------------------------------------------------------
|                                 DONATE HERO
|--------------------------------------------------------------------------
-->

  <section id="donate-hero">
    <div class="donate-large-header" class="large-header">
      <div class="donate-header-container">
        <div class="donate-header-text-wrapper">
          <h1 class="donate-main-title">Donate</span></h1>
        </div> <!-- donate-header-text -->
      </div> <!-- donate-header-box -->
    </div> <!-- #donate-large-header -->
  </section>



<!--
|--------------------------------------------------------------------------
|                                 DONATE
|--------------------------------------------------------------------------
-->

  <section id="donate-content">

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
          <div class="dontation-content-container">
            <?php
              while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'basic' );

              endwhile; // End of the loop.
            ?>
          </div> <!-- /.dontation-content-container -->
        </div> <!-- /.col -->
      </div> <!-- /.row -->
    </div> <!-- /.container-fluid -->

  </section>

<?php
get_footer();





