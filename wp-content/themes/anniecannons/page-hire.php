<?php
/*
  Template Name: Hire
*/

get_header(); ?>



<!--
|--------------------------------------------------------------------------
|                                 HIRE HERO
|--------------------------------------------------------------------------
-->

  <section id="hire-hero">
    <div class="hire-large-header" class="large-header">
      <video poster="video/skipping-stone.png" preload="auto" autoplay muted class="hire-video">
        <source src="<?php bloginfo('template_directory'); ?>/assets/images/hire/light-bulb.mp4" type="video/mp4">
      </video>
      <div class="hire-header-container">
        <div class="hire-header-text-wrapper">
          <h1 class="hire-main-title">Hire</span></h1>
        </div> <!-- /.hire-header-text-wrapper -->
      </div> <!-- /.hire-header-container -->
    </div> <!-- /.hire-large-header -->
  </section>



<!--
|--------------------------------------------------------------------------
|                                 HIRE
|--------------------------------------------------------------------------
-->

  <section id="hire-content">

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
          <div class="hire-content-container">
              <?php
                while ( have_posts() ) : the_post();

                  get_template_part( 'template-parts/content', 'basic' );

                endwhile; // End of the loop.
              ?>
          </div> <!-- /.hire-content-container -->
        </div> <!-- /.col -->
      </div> <!-- /.row -->
    </div> <!-- /.container-fluid -->

  </section>

<?php
get_footer();





